Written for use with geeqie as an "Edit" program. The idea is to select some files and then open a terminal with those file names already typed in to the prompt and with the cursor positioned at the beginning of the line, ready to type the name of a command to use to process them.

The 'terminal' script can be invoked with the first arg set to the name of a terminal to launch. Any terminal that accepts -e the way xterm does will work.

The way it works is by setting up a bash keybinding to run a function which then modifies a bash readline variable to change the prompt, the same way the prompt can be rewritten by tab completion, and then using xdotool to cause that key sequence to be typed as if by the user. Afterward the binding and the function are removed so as to not interfere with subsequent use of the terminal.

Dependencies: bash, xdotool

Bugs: The hard coded use of shift+F8 to trick bash in to updating the prompt will fail if it's bound to something else somewhere in X. You can choose another binding if this is a problem for you, but it was not set up in a way that makes that change pleasant.